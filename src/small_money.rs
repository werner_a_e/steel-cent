// Copyright 2016 John D. Hume
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use std::ops::{Add, Sub, Mul, Div, Rem, Neg};
use std::cmp::{Ordering, PartialOrd};
use std::i32;
use std::fmt;
use currency::Currency;
use formatting::{self, FormattableMoney, ParseableMoney};

/// A signed amount of money in a certain currency, with the currency's standard number of decimal
/// places.
///
/// The range of supported values is small enough to be inappropriate for some practical
/// applications. See `max` and `min` below.
///
/// Note that the arithmetic `ops` implementations all delegate to the `checked_` methods, panicking
/// on `None`, even in a `release` build. (This is in contrast to primitive ops, which will overflow
/// in a `release` build.)
///
/// A `SmallMoney` is 64 bits in size.
///
/// ```
/// # use steel_cent::SmallMoney;
/// assert_eq!(8, std::mem::size_of::<SmallMoney>());
/// ```
///
/// # Examples
///
/// ```
/// # use steel_cent::SmallMoney;
/// # use steel_cent::currency::USD;
/// #
/// let price = SmallMoney::of_major_minor(USD, 19, 95);
/// let shipping_and_handling = SmallMoney::of_major(USD, 10);
/// let convenience_charge = SmallMoney::of_major(USD, 6);
/// let discount: f64 = 1.0 - 0.2; // 20% off
/// let discounted_price = price * discount;
/// let fees = shipping_and_handling + convenience_charge;
/// let total = discounted_price + fees;
/// println!("price: {:?}, discounted_price: {:?}", price, discounted_price);
/// assert_eq!(SmallMoney::of_minor(USD, 1596), discounted_price);
/// assert_eq!(SmallMoney::of_minor(USD, 3196), total);
/// assert_eq!((price * discount) + shipping_and_handling + convenience_charge, total);
/// ```
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct SmallMoney {
    pub currency: Currency,
    amount_minor: i32,
}

impl SmallMoney {
    /// Creates a SmallMoney from its "minor" unit (e.g. US cents to USD).
    pub fn of_minor(currency: Currency, amount_minor: i32) -> Self {
        SmallMoney {
            currency: currency,
            amount_minor: amount_minor,
        }
    }

    /// Creates a SmallMoney from its "major" unit (e.g. US dollars to USD).
    pub fn of_major(currency: Currency, amount_major: i32) -> Self {
        Self::of_minor(currency, currency.major_to_minor_i32(amount_major))
    }

    pub fn of_major_minor(currency: Currency, amount_major: i32, amount_minor: i32) -> Self {
        Self::of_minor(currency,
                       currency.major_to_minor_i32(amount_major) + amount_minor)
    }

    pub fn zero(currency: Currency) -> Self {
        Self::of_minor(currency, 0)
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(SmallMoney::of_major_minor(USD, -21_474_836, -48), SmallMoney::min(USD));
    /// ```
    pub fn min(currency: Currency) -> Self {
        Self::of_minor(currency, i32::MIN)
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(SmallMoney::of_major_minor(USD, 21_474_836, 47), SmallMoney::max(USD));
    /// ```
    pub fn max(currency: Currency) -> Self {
        Self::of_minor(currency, i32::MAX)
    }

    /// Returns the major unit part of the amount.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(2, SmallMoney::of_minor(USD, 2_99).major_part());
    /// ```
    pub fn major_part(&self) -> i32 {
        self.currency.major_part_i32(self.amount_minor)
    }

    /// Returns the minor unit part of the amount.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(99, SmallMoney::of_minor(USD, 2_99).minor_part());
    /// ```
    pub fn minor_part(&self) -> i32 {
        self.currency.minor_part_i32(self.amount_minor)
    }

    /// Returns the total amount in the currency's minor unit.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// let two_dollars = SmallMoney::of_major(USD, 2);
    /// assert_eq!(200, two_dollars.minor_amount());
    /// ```
    pub fn minor_amount(&self) -> i32 {
        self.amount_minor
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::{GBP, JPY, USD};
    /// let five_usd = SmallMoney::of_major(USD, 5);
    /// let three_pound_seventy_five = SmallMoney::of_major_minor(GBP, 3, 75);
    /// let gbp_per_usd = 0.75;
    /// let five_eleven_yen = SmallMoney::of_major(JPY, 511);
    /// let jpy_per_usd = 102.15;
    /// assert_eq!(three_pound_seventy_five, five_usd.convert_to(GBP, gbp_per_usd));
    /// assert_eq!(five_usd, three_pound_seventy_five.convert_to(USD, gbp_per_usd.recip()));
    /// assert_eq!(510.75, 5.0 * jpy_per_usd); // but JPY has zero decimal places.
    /// assert_eq!(five_eleven_yen, five_usd.convert_to(JPY, jpy_per_usd)); // rounded.
    /// assert_eq!(five_usd, five_eleven_yen.convert_to(USD, jpy_per_usd.recip())); // rounded.
    /// ```
    pub fn convert_to(&self, currency: Currency, conversion_multiplier: f64) -> Self {
        let dec_adjust = 10f64
            .powi(currency.decimal_places() as i32 - self.currency.decimal_places() as i32);
        let amount = (self.amount_minor as f64 * conversion_multiplier * dec_adjust).round() as i32;
        Self::of_minor(currency, amount)
    }

    /// Returns absolute value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// let c = SmallMoney::of_major(USD, 100);
    /// assert_eq!(Some(c), c.checked_abs());
    /// assert_eq!(Some(c), (-c).checked_abs());
    /// assert_eq!(None, SmallMoney::min(USD).checked_abs());
    /// ```
    pub fn checked_abs(&self) -> Option<Self> {
        if self.amount_minor == i32::MIN {
            None
        } else if self.amount_minor.is_negative() {
            Some(Self::of_minor(self.currency, -self.amount_minor))
        } else {
            Some(*self)
        }
    }

    /// Returns absolute value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::USD;
    /// let c = SmallMoney::of_major(USD, 100);
    /// assert_eq!(c, c.abs());
    /// assert_eq!(c, (-c).abs());
    /// ```
    ///
    /// # Panics
    ///
    /// Panics for the minimum value.
    pub fn abs(&self) -> Self {
        self.checked_abs().expect("SmallMoney abs would overflow")
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_minor(USD, 4_01)),
    ///            SmallMoney::of_major(USD, 4).checked_add(SmallMoney::of_minor(USD, 1)));
    /// assert_eq!(None, SmallMoney::max(USD).checked_add(SmallMoney::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn checked_add(self, other: Self) -> Option<Self> {
        assert_eq!(self.currency, other.currency);
        self.amount_minor.checked_add(other.amount_minor)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_minor(USD, 3_99)),
    ///            SmallMoney::of_major(USD, 4).checked_sub(SmallMoney::of_minor(USD, 1)));
    /// assert_eq!(None, SmallMoney::min(USD).checked_sub(SmallMoney::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn checked_sub(self, other: Self) -> Option<Self> {
        assert_eq!(self.currency, other.currency);
        self.amount_minor.checked_sub(other.amount_minor)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_major(USD, 8)), SmallMoney::of_major(USD, 4).checked_mul(2));
    /// assert_eq!(None, SmallMoney::max(USD).checked_mul(2));
    /// assert_eq!(None, SmallMoney::min(USD).checked_mul(-1));
    /// ```
    pub fn checked_mul(self, n: i32) -> Option<Self> {
        self.amount_minor.checked_mul(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// Checked multiplication by a float with rounding.
    /// Note that a float has less-than-integer precision for very large and very small
    /// amounts of money, which can result in surprising rounding errors.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_minor(USD, 8_40)), SmallMoney::of_major(USD, 4).checked_mul_f(2.1));
    /// assert_eq!(None, SmallMoney::max(USD).checked_mul_f(1.01));
    /// assert_eq!(None, SmallMoney::min(USD).checked_mul_f(-1.0));
    /// ```
    pub fn checked_mul_f(self, n: f64) -> Option<Self> {
        let candidate_amount_minor = self.amount_minor as f64 * n;
        let min = ::std::i32::MIN as f64;
        let max = ::std::i32::MAX as f64;
        if (min < candidate_amount_minor) && (candidate_amount_minor < max) {
            Some(Self::of_minor(self.currency, candidate_amount_minor.round() as i32))
        } else {
            None
        }
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_major(USD, 2)), SmallMoney::of_minor(USD, 4_01).checked_div(2));
    /// assert_eq!(None, SmallMoney::of_major(USD, 1).checked_div(0));
    /// assert_eq!(None, SmallMoney::min(USD).checked_div(-1));
    /// ```
    pub fn checked_div(self, n: i32) -> Option<Self> {
        self.amount_minor.checked_div(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_minor(USD, 1)), SmallMoney::of_minor(USD, 4_01).checked_rem(2));
    /// assert_eq!(None, SmallMoney::of_major(USD, 1).checked_rem(0));
    /// assert_eq!(None, SmallMoney::min(USD).checked_rem(-1));
    /// ```
    pub fn checked_rem(self, n: i32) -> Option<Self> {
        self.amount_minor.checked_rem(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// Negates the value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(SmallMoney::of_major(USD, -1)), SmallMoney::of_major(USD, 1).checked_neg());
    /// assert_eq!(None, SmallMoney::min(USD).checked_neg());
    /// ```
    pub fn checked_neg(self) -> Option<Self> {
        self.amount_minor.checked_neg()
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(SmallMoney::of_minor(USD, 4_01),
    ///            SmallMoney::of_major(USD, 4).saturating_add(SmallMoney::of_minor(USD, 1)));
    /// assert_eq!(SmallMoney::max(USD),
    ///            SmallMoney::max(USD).saturating_add(SmallMoney::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn saturating_add(self, other: Self) -> Self {
        assert_eq!(self.currency, other.currency);
        Self::of_minor(self.currency, self.amount_minor.saturating_add(other.amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(SmallMoney::of_minor(USD, 3_99),
    ///            SmallMoney::of_major(USD, 4).saturating_sub(SmallMoney::of_minor(USD, 1)));
    /// assert_eq!(SmallMoney::min(USD),
    ///            SmallMoney::min(USD).saturating_sub(SmallMoney::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn saturating_sub(self, other: Self) -> Self {
        assert_eq!(self.currency, other.currency);
        Self::of_minor(self.currency, self.amount_minor.saturating_sub(other.amount_minor))
    }

    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(SmallMoney::of_major(USD, 8), SmallMoney::of_major(USD, 4).saturating_mul(2));
    /// assert_eq!(SmallMoney::max(USD), SmallMoney::max(USD).saturating_mul(2));
    /// assert_eq!(SmallMoney::max(USD), SmallMoney::min(USD).saturating_mul(-1));
    /// ```
    pub fn saturating_mul(self, n: i32) -> Self {
        Self::of_minor(self.currency, self.amount_minor.saturating_mul(n))
    }
}

impl fmt::Display for SmallMoney {
    /// Displays the value with `formatting::STYLE_GENERIC`. See the `formatting` module for other
    /// pre-defined and custom styles.
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!("1,234.56\u{a0}GBP", format!("{}", &SmallMoney::of_minor(GBP, 123456)));
    /// ```
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{}",
               formatting::format(formatting::generic_style(), self))
    }
}

impl FormattableMoney for SmallMoney {
    fn unformatted_minor_amount(&self) -> String {
        format!("{}", self.amount_minor)
    }

    fn currency(&self) -> Currency {
        self.currency
    }
}

impl ParseableMoney for SmallMoney {
    fn from_unformatted_minor_amount(currency: Currency, unformatted_minor_amount: &str)
                                     -> Result<Self, ::std::num::ParseIntError> {
        Ok(Self::of_minor(currency, try!(unformatted_minor_amount.parse::<i32>())))
    }
}

impl Add for SmallMoney {
    type Output = SmallMoney;
    /// Adds two `SmallMoney`s.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let one = SmallMoney::of_major(USD, 1);
    /// assert_eq!(SmallMoney::of_major(USD, 2), one + one);
    /// // Likewise for refs:
    /// assert_eq!(SmallMoney::of_major(USD, 2), one + &one);
    /// assert_eq!(SmallMoney::of_major(USD, 2), &one + one);
    /// assert_eq!(SmallMoney::of_major(USD, 2), &one + &one);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_major(USD, 1) + SmallMoney::of_major(JPY, 1); // panics!
    /// ```
    ///
    /// Panics when addition of minor amounts would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MAX) + SmallMoney::of_minor(USD, 1); // panics!
    /// ```
    fn add(self, other: SmallMoney) -> SmallMoney {
        self.checked_add(other).expect("SmallMoney overflow")
    }
}

forward_ref_binop!{ impl Add, add for SmallMoney, SmallMoney }

impl Sub for SmallMoney {
    type Output = SmallMoney;
    /// Subtracts one `SmallMoney` from another.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let one = SmallMoney::of_major(USD, 1);
    /// assert_eq!(SmallMoney::of_major(USD, 0), one - one);
    /// // Likewise for refs:
    /// assert_eq!(SmallMoney::of_major(USD, 0), one - &one);
    /// assert_eq!(SmallMoney::of_major(USD, 0), &one - one);
    /// assert_eq!(SmallMoney::of_major(USD, 0), &one - &one);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_major(USD, 1) - SmallMoney::of_major(JPY, 1); // panics!
    /// ```
    ///
    /// Panics when subtraction of minor amounts would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MIN) - SmallMoney::of_minor(USD, 1); // panics!
    /// ```
    fn sub(self, other: SmallMoney) -> SmallMoney {
        self.checked_sub(other).expect("SmallMoney sub would overflow")
    }
}

forward_ref_binop!{ impl Sub, sub for SmallMoney, SmallMoney }

impl Mul<i32> for SmallMoney {
    type Output = SmallMoney;
    /// Multiplies money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let two_usd = SmallMoney::of_major(USD, 2);
    /// let four_usd = SmallMoney::of_major(USD, 4);
    /// assert_eq!(four_usd, two_usd * 2);
    /// // Likewise for refs:
    /// assert_eq!(four_usd, two_usd * &2);
    /// assert_eq!(four_usd, &two_usd * 2);
    /// assert_eq!(four_usd, &two_usd * &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when multiplication of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MAX) * 2; // panics!
    /// ```
    fn mul(self, n: i32) -> SmallMoney {
        self.checked_mul(n).expect("SmallMoney mul would overflow.")
    }
}

forward_ref_binop!{ impl Mul, mul for SmallMoney, i32 }

impl Mul<f64> for SmallMoney {
    type Output = SmallMoney;
    /// Multiplies money by a float, rounding if needed.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let one = SmallMoney::of_major(USD, 1);
    /// let one01 = SmallMoney::of_minor(USD, 1_01);
    /// assert_eq!(one01, one * 1.005001);
    /// assert_eq!(SmallMoney::of_minor(USD, 1_005_00), SmallMoney::of_major(USD, 1_000) * 1.005001);
    /// assert_eq!(SmallMoney::of_minor(USD, 10_050_01), SmallMoney::of_major(USD, 10_000) * 1.005001);
    /// // Likewise for refs:
    /// assert_eq!(one01, one * &1.005001);
    /// assert_eq!(one01, &one * 1.005001);
    /// assert_eq!(one01, &one * &1.005001);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when multiplication of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MAX) * 1.01; // panics!
    /// ```
    fn mul(self, n: f64) -> SmallMoney {
        self.checked_mul_f(n).expect("SmallMoney mul would overflow.")
    }
}

forward_ref_binop!{ impl Mul, mul for SmallMoney, f64 }

impl Div<i32> for SmallMoney {
    type Output = SmallMoney;
    /// Divides money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let two_usd = SmallMoney::of_major(USD, 2);
    /// let four01 = SmallMoney::of_minor(USD, 4_01);
    /// assert_eq!(two_usd, four01 / 2);
    /// // Likewise for refs:
    /// assert_eq!(two_usd, &four01 / 2);
    /// assert_eq!(two_usd, four01 / &2);
    /// assert_eq!(two_usd, &four01 / &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when division of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MIN) / -1; // panics!
    /// ```
    ///
    /// Panics when `n` is zero.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, 1) / 0; // panics!
    /// ```
    fn div(self, n: i32) -> SmallMoney {
        self.checked_div(n).expect(if n == 0 {
            "SmallMoney div by zero"
        } else {
            "SmallMoney div would overflow"
        })
    }
}

forward_ref_binop!{ impl Div, div for SmallMoney, i32 }

impl Rem<i32> for SmallMoney {
    type Output = SmallMoney;
    /// Remainder of dividing money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// let one_cent = SmallMoney::of_minor(USD, 1);
    /// let four01 = SmallMoney::of_minor(USD, 4_01);
    /// assert_eq!(one_cent, four01 % 2);
    /// // Likewise for refs:
    /// assert_eq!(one_cent, &four01 % 2);
    /// assert_eq!(one_cent, four01 % &2);
    /// assert_eq!(one_cent, &four01 % &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when division of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, std::i32::MIN) % -1; // panics!
    /// ```
    ///
    /// Panics when `n` is zero.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// SmallMoney::of_minor(USD, 1) % 0; // panics!
    /// ```
    fn rem(self, n: i32) -> SmallMoney {
        self.checked_rem(n).expect(if n == 0 {
            "SmallMoney rem by zero"
        } else {
            "SmallMoney rem would overflow"
        })
    }
}

forward_ref_binop!{ impl Rem, rem for SmallMoney, i32 }

impl Neg for SmallMoney {
    type Output = SmallMoney;
    /// Negation.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(SmallMoney::of_minor(USD, -1), -SmallMoney::of_minor(USD, 1));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when negation would overflow, which is only the case for the minimum value.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// -SmallMoney::min(USD); // panics!
    /// ```
    fn neg(self) -> SmallMoney {
        self.checked_neg().expect("SmallMoney neg would overflow")
    }
}

impl<'a> Neg for &'a SmallMoney {
    type Output = SmallMoney;
    /// Negation of ref.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// assert_eq!(SmallMoney::of_minor(USD, -1), -(&SmallMoney::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when negation would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::SmallMoney;
    /// # use steel_cent::currency::*;
    /// -(&SmallMoney::of_minor(USD, std::i32::MIN)); // panics!
    /// ```
    fn neg(self) -> SmallMoney {
        (*self).checked_neg().expect("SmallMoney neg would overflow")
    }
}

/// Compares to `SmallMoney` of the same currency only.
///
/// Returns `None` when currencies differ, causing all comparison operators to return false when
/// currencies differ.
impl PartialOrd for SmallMoney {
    fn partial_cmp(&self, other: &SmallMoney) -> Option<Ordering> {
        if self.currency == other.currency {
            self.amount_minor.partial_cmp(&other.amount_minor)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use currency::{EUR, JPY, USD};
    use super::SmallMoney;
    use std::cmp::Ordering;

    #[test]
    fn compares_equal() {
        let one_dollar = SmallMoney::of_minor(USD, 100);
        assert_eq!(one_dollar, SmallMoney::of_minor(USD, 100));
        assert!(one_dollar != SmallMoney::of_minor(USD, 101));
    }

    #[test]
    fn converts_major_to_minor() {
        assert_eq!(SmallMoney::of_minor(USD, 100), SmallMoney::of_major(USD, 1));
        assert_eq!(SmallMoney::of_minor(JPY, 100), SmallMoney::of_major(JPY, 100));
    }

    #[test]
    fn exposes_minor_part() {
        assert_eq!(34, SmallMoney::of_minor(USD, 1234).minor_part());
        assert_eq!(-34, SmallMoney::of_minor(USD, -1234).minor_part());
        assert_eq!(0, SmallMoney::of_minor(JPY, 1234).minor_part());
        assert_eq!(0, SmallMoney::of_minor(JPY, -1234).minor_part());
    }

    #[test]
    fn exposes_major_part() {
        assert_eq!(12, SmallMoney::of_minor(USD, 1234).major_part());
        assert_eq!(-12, SmallMoney::of_minor(USD, -1234).major_part());
        assert_eq!(1234, SmallMoney::of_minor(JPY, 1234).major_part());
        assert_eq!(-1234, SmallMoney::of_minor(JPY, -1234).major_part());
    }

    // -----------------
    // Custom Ops impls

    #[test]
    fn multiplies_by_int_or_float() {
        assert_eq!(SmallMoney::of_minor(USD, 400), &SmallMoney::of_minor(USD, 200) * 2);
        assert_eq!(SmallMoney::of_minor(USD, 180), &SmallMoney::of_minor(USD, 200) * 0.9);
    }

    #[test]
    fn multiplies_all_combinations_of_ref_and_consumed() {
        let two_usd = SmallMoney::of_major(USD, 2);
        assert_eq!(SmallMoney::of_major(USD, 4), two_usd.clone() * 2);
        assert_eq!(SmallMoney::of_major(USD, 1), two_usd.clone() * 0.5);
        assert_eq!(SmallMoney::of_major(USD, 4), two_usd.clone() * &2);
        assert_eq!(SmallMoney::of_major(USD, 1), two_usd.clone() * &0.5);
        assert_eq!(SmallMoney::of_major(USD, 4), &two_usd * 2);
        assert_eq!(SmallMoney::of_major(USD, 1), &two_usd * 0.5);
        assert_eq!(SmallMoney::of_major(USD, 4), &two_usd * &2);
        assert_eq!(SmallMoney::of_major(USD, 1), &two_usd * &0.5);
    }

    #[test]
    fn divide_and_remainder_by_int() {
        let ten_usd = SmallMoney::of_major(USD, 10);
        let three_thirty_three_usd = SmallMoney::of_minor(USD, 333);
        let one_cent = SmallMoney::of_minor(USD, 1);
        assert_eq!(three_thirty_three_usd, ten_usd.clone() / 3);
        assert_eq!(three_thirty_three_usd, ten_usd.clone() / &3);
        assert_eq!(three_thirty_three_usd, &ten_usd / 3);
        assert_eq!(three_thirty_three_usd, &ten_usd / &3);
        assert_eq!(one_cent, ten_usd.clone() % 3);
        assert_eq!(one_cent, ten_usd.clone() % &3);
        assert_eq!(one_cent, &ten_usd % 3);
        assert_eq!(one_cent, &ten_usd % &3);
    }

    #[test]
    fn unary_negation() {
        let one_dollar = SmallMoney::of_major(USD, 1);
        assert_eq!(SmallMoney::of_major(USD, -1), -&one_dollar);
        assert_eq!(SmallMoney::of_major(USD, -1), -one_dollar);
    }

    // ----------
    // cmp

    #[test]
    fn compares_when_currencies_match() {
        let one_dollar = SmallMoney::of_major(USD, 1);
        let two_dollars = SmallMoney::of_major(USD, 2);
        assert_eq!(Some(Ordering::Less), one_dollar.partial_cmp(&two_dollars));
        assert_eq!(Some(Ordering::Equal), one_dollar.partial_cmp(&one_dollar));
        assert_eq!(Some(Ordering::Greater),
                   two_dollars.partial_cmp(&one_dollar));
        assert!(one_dollar < two_dollars);
        assert!(one_dollar <= two_dollars);
        assert!(one_dollar <= one_dollar);
        assert!(two_dollars > one_dollar);
        assert!(two_dollars >= two_dollars);
        assert!(two_dollars >= two_dollars);
    }

    #[test]
    fn does_not_compare_when_currencies_differ() {
        let one_dollar = SmallMoney::of_major(USD, 1);
        let one_euro = SmallMoney::of_major(EUR, 1);
        assert_eq!(None, one_dollar.partial_cmp(&one_euro));
        assert_eq!(None, one_euro.partial_cmp(&one_dollar));
        assert!(!(one_dollar < one_euro));
        assert!(!(one_euro < one_dollar));
        assert!(!(one_dollar <= one_euro));
        assert!(!(one_euro <= one_dollar));
        assert!(!(one_dollar > one_euro));
        assert!(!(one_euro > one_dollar));
        assert!(!(one_dollar >= one_euro));
        assert!(!(one_euro >= one_dollar));
    }
}
